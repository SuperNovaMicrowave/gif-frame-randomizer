#!/bin/python

import sys
import argparse

from PIL import Image
from PIL import GifImagePlugin

import random


def main(args):
    arg_parser = argparse.ArgumentParser(description='Randomize the frame order of gifs.')
    
    arg_parser.add_argument('-t', '--frame-time', type=int,
                            help='frametime in milliseconds (default=10)')
    arg_parser.add_argument('-c', '--loop-count', type=int,
                            help='loop count; 0 = infinite (default=0)')
    arg_parser.add_argument('-q', '--quality', type=int,
                            help='compression quality (default=95)')
    arg_parser.add_argument('-o', '--optimize', action='store_true',
                            help='optimize gif (default=False)')
    
    arg_parser.add_argument('INPUT_FILE')
    arg_parser.add_argument('OUTPUT_FILE')
    
    parsed_args = arg_parser.parse_args(args)
    
    input_path = parsed_args.INPUT_FILE
    output_path = parsed_args.OUTPUT_FILE
    
    frame_time = parsed_args.frame_time
    if frame_time == None:
        frame_time = 10
    loop_count = parsed_args.loop_count
    if loop_count == None:
        loop_count = 0
    quality = parsed_args.quality
    if quality == None:
        quality = 95

    optimize = parsed_args.optimize
    if optimize == None:
        optimize = False
    
    original_gif = Image.open(input_path)
    frame_count = original_gif.n_frames
    
    frames = []
    
    for i in range(0, frame_count):
        original_gif.seek(i)
        frames.append(original_gif.copy())
    
    random.shuffle(frames)
    
    frames[0].save(output_path,
                   save_all=True,
                   append_images=frames[1:],
                   duration=frame_time,
                   loop=loop_count,
                   optimize=optimize,
                   quality=quality)


if __name__ == "__main__":
    argv = sys.argv
    if argv.__len__() <= 1:
        main(['-h'])
    else:
        main(argv[1:])
