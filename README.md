## GIF Randomizer

A cli tool to randomize the order of frames in an animated GIF file.

## Installation

1. install [python3](https://www.python.org/downloads/release/python-385/)

2. clone the repository and open a terminal in the folder.

_If you don't want to use virtualenv, just skip to step 6!_

3. run `pip install virtualenv`

4. run `virtualenv venv`

5.
    - on Windows/Powershell: run `.\venv\Scripts\activate.ps1`
    - on Windows/cmd: run `.\venv\Scripts\activate.bat`
    - on bash: run `source .\venv\bin\activate`
    - on fish: run `source .\venv\bin\activate.fish`
    - ...

6. execute `pip install -r build_requirements.txt`

7. run `build.bat` on Windows or `build.sh` on OSX/Linux

8. add the executable in the dist folder to your path if you want to use it system-wide.